# Install on Ubuntu

```
curl https://gitlab.com/Dundek/private-server-hub/raw/master/install.sh | bash
```

# Deploy Process

1. Docker compose down
2. Generate docker-compose yaml file
3. Generate nginx config file
4. Docker compose up

